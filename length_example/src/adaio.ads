with System;

package Adaio is

   procedure Print (Msg : System.Address; Max_Len : Integer)
     with
       Export,
       Convention => C,
       External_Name => "print";

end Adaio;
