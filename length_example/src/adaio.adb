with Ada.Text_Io;
with String_Utils;

package body Adaio is

   procedure Print (Msg : System.Address; Max_Len : Integer) is
      Message : String := String_Utils.Convert_To_Ada (Msg,
                                                       "Empty String",
                                                       Max_Len);
   begin
      Ada.Text_Io.Put_Line (Integer'Image (Message'Length) & ": " & Message);
   end Print;

end Adaio;
