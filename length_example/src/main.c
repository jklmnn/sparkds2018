
extern void adainit(void);
extern void adafinal(void);
extern void print(char *, int);

int main(int argc, char *argv[])
{
    char *message = 0;

    if(argc == 2){
        message = argv[1];
    }

    adainit();
    print(message, 128);
    adafinal();

    return 0;
}
