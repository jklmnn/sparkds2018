
# Resources

- [GitHub](https://github.com/jklmnn)
- [Componolit GmbH](https://componolit.com/)
- [Componolit GitHub](https://github.com/Componolit)
- [AdaCore Ada and SPARK learning platform](https://learn.adacore.com/)
- [GNAT community edition](https://www.adacore.com/download/)
- [Muen microkernel](https://muen.sk/)
- [WooKey USB](https://www.sstic.org/2018/presentation/wookey_usb_devices_strike_back/)
- [WooKey USB (PDF)](https://www.sstic.org/media/SSTIC2018/SSTIC-actes/wookey_usb_devices_strike_back/SSTIC2018-Article-wookey_usb_devices_strike_back-michelizza_lefaure_renard_thierry_trebuchet_benadjila_saV2IIT.pdf)

